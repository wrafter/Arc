To setup the project and program an ATMEG16U2 with the Wrafter ARC USB DFU bootloader:
1. unpack the source into LUFA's (version 100807) Bootloader directory
2. set ARDUINO_MODEL_PID in the makefile as appropriate
3. do "make clean; make; make program"

Check that the board enumerates as "Wrafter ARC DFU".  Test by uploading the Wrafter_ARC-usbserial application firmware (see instructions in Wrafter_ARC-usbserial directory)

