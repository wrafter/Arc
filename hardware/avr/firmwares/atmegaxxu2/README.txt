Wrafter ARC Firmwares for the ATmega16U2

This directory contains the firmwares used on the ATmega16U2 on the Wrafter ARC.
The wrafter_arc-usbdfu directory contains the DFU bootloader on the 16U2;
the wrafter_arc-usbserial directory contains the actual usb to serial firmware.
Both should be compiled against LUFA 100807.  The two .hex files in this
directory combine the dfu and serial firmwares into a single file to burn onto
the 16U2.

To burn (ARC):
avrdude -p m16u2 -F -P usb -c avrispmkii -U flash:w:ARC-dfu_and_usbserial_combined.hex -U lfuse:w:0xFF:m -U hfuse:w:0xD9:m -U efuse:w:0xF4:m -U lock:w:0x0F:m


Note on USB Vendor IDs (VID) and Product IDs (PID): The arduino-usbdfu
project uses Atmel's VID and MCU-specific PIDs to maintain compatibility
with their FLIP software.  The source code to the arduino-usbserial
project includes Atmel's VID and a PID donated by them to LUFA.  This
PID is used in LUFA's USBtoSerial project, which forms the basis for
arduino-usbserial.  According to the LUFA documentation, this VID/PID
combination is:

 "For use in testing of LUFA powered devices during development only,
 by non-commercial entities. All devices must accept collisions on this
 VID/PID range (from other in-development LUFA devices) to be resolved
 by using a unique release number in the Device Descriptor. No devices
 using this VID/PID combination may be released to the general public."

The production version of the arduino-usbserial firmware uses the
Arduino VID.  This is only for use with official Arduino hardware and
should not be used on other products.
