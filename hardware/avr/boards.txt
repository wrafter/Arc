# See: http://code.google.com/p/arduino/wiki/Platforms

menu.cpu=Processor

##############################################################

arc.name=Wrafter ARC

arc.vid.0=0x1209
arc.pid.0=0xF000

arc.upload.tool=avrdude
arc.upload.protocol=arduino
arc.upload.maximum_size=57344
arc.upload.maximum_data_size=4096
arc.upload.speed=115200

arc.bootloader.tool=avrdude
arc.bootloader.low_fuses=0xFF
arc.bootloader.high_fuses=0xDF
arc.bootloader.extended_fuses=0x3D
arc.bootloader.unlock_bits=0x3F
arc.bootloader.lock_bits=0x2F
arc.bootloader.file=wrafter_arc/optiboot_wrafter_arc.hex

arc.build.mcu=atmega64m1
arc.build.f_cpu=16000000L
arc.build.board=AVR_arc
arc.build.core=wrafter_arc
arc.build.variant=Wrafter_ARC

##############################################################

arcbeta.name=Wrafter ARC beta

arcbeta.vid.0=0x1209
arcbeta.pid.0=0xF000

arcbeta.upload.tool=avrdude
arcbeta.upload.protocol=arduino
arcbeta.upload.maximum_size=31744
arcbeta.upload.maximum_data_size=2048
arcbeta.upload.speed=115200

arcbeta.bootloader.tool=avrdude
arcbeta.bootloader.low_fuses=0xFF
arcbeta.bootloader.high_fuses=0xDD
arcbeta.bootloader.extended_fuses=0x3D
arcbeta.bootloader.unlock_bits=0x3F
arcbeta.bootloader.lock_bits=0x2F
arcbeta.bootloader.file=wrafter_arc/optiboot_wrafter_arc_beta.hex

arcbeta.build.mcu=atmega32m1
arcbeta.build.f_cpu=16000000L
arcbeta.build.board=AVR_arc_beta
arcbeta.build.core=wrafter_arc
arcbeta.build.variant=Wrafter_ARC

##############################################################
