/*
  pins_arduino.h - Pin definition functions for Arduino
  Part of Arduino - http://www.arduino.cc/

  Copyright (c) 2007 David A. Mellis

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General
  Public License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place, Suite 330,
  Boston, MA  02111-1307  USA
*/

#ifndef Pins_Arduino_h
#define Pins_Arduino_h

#include <avr/pgmspace.h>

#define NUM_DIGITAL_PINS            24
#define NUM_ANALOG_INPUTS           6

#define analogInputToDigitalPin(p)  ((p < 6) ? (p) + 18 : -1)
#define digitalPinHasPWM(p)         ((p) == 1 || (p) == 6 || (p) == 9)

// __AVR_ATmega32M1__ & __AVR_ATmega64M1__ have an unusual mapping of pins to channels and needs extra lookup table
extern const uint8_t PROGMEM analog_pin_to_channel_PGM[];
#define analogPinToChannel(P)  ( pgm_read_byte( analog_pin_to_channel_PGM + (P) ) )

static const uint8_t SS   = 10;
static const uint8_t MOSI = 11;
static const uint8_t MISO = 12;
static const uint8_t SCK  = 13;

static const uint8_t SDA = 21;
static const uint8_t SCL = 22;
#define LED_BUILTIN 13

static const uint8_t A0 = 18;
static const uint8_t A1 = 19;
static const uint8_t A2 = 20;
static const uint8_t A3 = 21;
static const uint8_t A4 = 22;
static const uint8_t A5 = 23;
static const uint8_t A6 = 24; //IO0
static const uint8_t A7 = 25; //IO2
static const uint8_t A8 = 26; //IO5
static const uint8_t A9 = 27; //IO13
static const uint8_t A10 = 28; //temperature sensor
static const uint8_t A11 = 29; //VCC/4

static const uint8_t AD0 = 30; //AMP0
static const uint8_t AD1 = 31; //AMP1
static const uint8_t AD2 = 32; //AMP2

static const uint8_t AC0 = 33; //Analog Comparator 0
static const uint8_t AC1 = 34; //Analog Comparator 1
static const uint8_t AC2 = 35; //Analog Comparator 2
static const uint8_t AC3 = 36; //Analog Comparator 3

static const uint8_t DAC_PORT = 14;
static const uint8_t DAC_INTERNAL = 37;

#define GAIN5  0
#define GAIN10 1
#define GAIN20 2
#define GAIN40 3

//Positive input from pin
#define VREF1  0 //“VREF”/6.40 = 0.78125 V
#define VREF2  1 //“VREF”/3.20 = 1.56250 V
#define VREF3  2 //“VREF”/2.13 = 2.34742 V
#define VREF4  3 //“VREF”/1.60 = 3.12500 V
#define BGAP   4 //Bandgap (1.1V)
#define DAC_IN 5 //DAC result
#define NORMAL 6 //Analog comparator negative input (ACMPM pin)
//Positive input from amplifier
#define AD_VREF1  8  //“VREF”/6.40 = 0.78125 V
#define AD_VREF2  9  //“VREF”/3.20 = 1.56250 V
#define AD_VREF3  10 //“VREF”/2.13 = 2.34742 V
#define AD_VREF4  11 //“VREF”/1.60 = 3.12500 V
#define AD_BGAP   12 //Bandgap (1.1V)
#define AD_DAC_IN 13 //DAC result
#define AD_NORMAL 14 //Analog comparator negative input (ACMPM pin)

#define digitalPinToPCICR(p)    (((p) >= 0 && (p) <= 23) ? (&PCICR) : ((uint8_t *)0))

#define digitalPinToPCICRbit(p) (((p) == 2) || ((p) == 4)  || \
								((p) == 5)  || ((p) == 11) || \
								((p) == 12) || ((p) == 13) || \
								((p) == 15) || ((p) == 19) || \
									? 0 : \
								(((p) == 3) || ((p) == 9)  || \
								((p) == 14) || ((p) == 16) || \
								((p) == 17) || ((p) == 18) || \
								((p) == 22) || ((p) == 23) || \
									? 1 : \
								2))

#define digitalPinToPCMSK(p)    (((p) == 2) || ((p) == 4)  || \
								((p) == 5)  || ((p) == 11) || \
								((p) == 12) || ((p) == 13) || \
								((p) == 15) || ((p) == 19) || \
									? (&PCMSK0) : \
								(((p) == 3) || ((p) == 9)  || \
								((p) == 14) || ((p) == 16) || \
								((p) == 17) || ((p) == 18) || \
								((p) == 22) || ((p) == 23) || \
									? (&PCMSK1) : \
								(((p) == 0) || ((p) == 1)  || \
								((p) == 6)  || ((p) == 7)  || \
								((p) == 8)  || ((p) == 10) || \
								((p) == 20) || ((p) == 21) || \
									? (&PCMSK2) : \
								((uint8_t *)0)))

#define digitalPinToPCMSKbit(p) (((p) == 2) || ((p) == 3) || ((p) == 0) \
									? 0 : \
								(((p) == 4) || ((p) == 9) || ((p) == 1) \
									? 1 : \
								(((p) == 5) || ((p) == 14) || ((p) == 6) \
									? 2 : \
								(((p) == 11) || ((p) == 16) || ((p) == 7) \
									? 3 : \
								(((p) == 12) || ((p) == 17) || ((p) == 8) \
									? 4 : \
								(((p) == 13) || ((p) == 18) || ((p) == 10) \
									? 5 : \
								(((p) == 15) || ((p) == 22) || ((p) == 20) \
									? 6 : \
								7))

// Wrafter ARC has interrupts on pins 2, 3, A1 (19), and A3 (21)
#define digitalPinToInterrupt(p) ((p) == 2 ? 2 : ((p) == 3 ? 3 : ((p) == 19 ? 1 : ((p) == 21 ? 0 : NOT_AN_INTERRUPT))))

#ifdef ARDUINO_MAIN

// On the Arduino board, digital pins are also used
// for the analog output (software PWM).  Analog input
// pins are a separate set.
//
//Port		pin		use									Arduino pin
//
//VCC		4		VCC									5v
//GND		5		GND									gnd
//AVCC		19		AVCC								--
//AGND		20		AGND								--
//AREF		21		AREF								aref
//PB0		8		PSCOUT2A / MISO						12
//PB1		9		PSCOUT2B / MOSI						11
//PB2		16		ADC5 / INT1 / ACMPN0				A1 (19)
//PB3		23		AMP0-								4
//PB4		24		AMP0+								E1 (15)
//PB5		26		ADC6 / INT2 / ACMPN1 / AMP2-		2
//PB6		27		PSCOUT1B							5
//PB7		28		PSCOUT0B / SCK						13
//PC0		30		PSCOUT1A / INT3						3
//PC1		3		PCSIN1 / OC1B / SS_A				9
//PC2		6		TXCAN								E3 (17)
//PC3		7		RXCAN								E2 (16)
//PC4		17		ADC8 / AMP1- / ACMPN3 / (SCL)		A5 (23)
//PC5		18		ADC9 / AMP1+ / ACMP3 / (SDA)		A4 (22)
//PC6		22		ADC10 / ACMP1						A0 (18)
//PC7		25		D2A / AMP2+							E0 (14)
//PD0		29		PSCOUT0A							10
//PD1		32		PSCIN0								8
//PD2		1		OC1A / PSCIN2 / MISO_A				6
//PD3		2		TXD / TXLIN / SS / OC0A / MOSI_A	1
//PD4		12		RXD / RXLIN / SCK_A					0
//PD5		13		ADC2 / ACMP2						A2 (20)
//PD6		14		ADC3 / ACMPN2 / INT0				A3 (21)
//PD7		15		ACMP0								7
//PE0		31		RESET								reset
//PE1		10		XTAL1								--
//PE2		11		XTAL2 / ADC0						--
//
//
//pin	port	arduino		coms				int				PSC				Analog	An comp.	Diff amp
//
//13	PB7		led / sck	SCK					PCINT7				PSCOUT0B	ADC4
//12	PB0		miso		MISO				PCINT0				PSCOUT2A
//11	PB1		pwm / mosi	MOSI				PCINT1				PSCOUT2B
//10	PD0		pwm / ss	(SS)				PCINT16				PSCOUT0A
//9		PC1		pwm			SS_A				PCINT9				PSCIN1		OC1B
//8		PD1										PCINT17				PSCIN0
//7		PD7										PCINT23							ACMP0
//6		PD2		pwm			MISO_A				PCINT18				PSCIN2		OC1A
//5		PB6		pwm								PCINT6				PSCOUT1B	ADC7
//4		PB3										PCINT3												AMP0-
//3		PC0		pwm / int						INT3 / PCINT8		PSCOUT1A
//2		PB5										INT2 / PCINT5					ADC6	ACMPN1		AMP2-
//1		PD3		tx			TXD / SS / MOSI_A	PCINT19							OC0A
//0		PD4		rx			RXD / SCK_A			PCINT20							ADC1
//
//E3	PC2		extra		TXCAN				PCINT10
//E2	PC3		extra		RXCAN				PCINT11
//E1	PB4		extra							PCINT4												AMP0+
//E0	PC7		extra							PCINT15							D2A					AMP2+
//
//A5	PC4		SCL			--					PCINT12							ADC8	ACMPN3		AMP1-
//A4	PC5		SDA			--					PCINT13							ADC9	ACMP3		AMP1+
//A3	PD6										INT0 / PCINT22					ADC3	ACMPN2
//A2	PD5										PCINT21							ADC2	ACMP2
//A1	PB2		int								INT1 / PCINT2					ADC5	ACMPN0
//A0	PC6										PCINT14							ADC10	ACMP1

// these arrays map port names (e.g. port B) to the
// appropriate addresses for various functions (e.g. reading
// and writing)
const uint16_t PROGMEM port_to_mode_PGM[] = {
	NOT_A_PORT,
	NOT_A_PORT,
	(uint16_t) &DDRB,
	(uint16_t) &DDRC,
	(uint16_t) &DDRD,
};

const uint16_t PROGMEM port_to_output_PGM[] = {
	NOT_A_PORT,
	NOT_A_PORT,
	(uint16_t) &PORTB,
	(uint16_t) &PORTC,
	(uint16_t) &PORTD,
};

const uint16_t PROGMEM port_to_input_PGM[] = {
	NOT_A_PORT,
	NOT_A_PORT,
	(uint16_t) &PINB,
	(uint16_t) &PINC,
	(uint16_t) &PIND,
};

const uint8_t PROGMEM digital_pin_to_port_PGM[] = {
	PD, /* 0 */
	PD,
	PB,
	PC,
	PB,
	PB,
	PD,
	PD,

	PD, /* 8 */
	PC,
	PD,
	PB,
	PB,
	PB,
	PC,
	PB,

	PC, /* 16 */
	PC,
	PC,
	PB,
	PD,
	PD,
	PC,
	PC,
};

const uint8_t PROGMEM digital_pin_to_bit_mask_PGM[] = {
	_BV(4), /* 0 */
	_BV(3),
	_BV(5),
	_BV(0),
	_BV(3),
	_BV(6),
	_BV(2),
	_BV(7),

	_BV(1), /* 8 */
	_BV(1),
	_BV(0),
	_BV(1),
	_BV(0),
	_BV(7),
	_BV(7),
	_BV(4),

	_BV(3), /* 16 */
	_BV(2),
	_BV(6),
	_BV(2),
	_BV(5),
	_BV(6),
	_BV(5),
	_BV(4),
};

const uint8_t PROGMEM digital_pin_to_timer_PGM[] = {
	NOT_ON_TIMER, /* 0 */
	TIMER0A,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	TIMER1A,
	NOT_ON_TIMER,
	NOT_ON_TIMER, /* 8 */
	TIMER1B,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER, /* 16 */
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER, /* 23 */
};

const uint8_t PROGMEM analog_pin_to_channel_PGM[15] = {
	10,	// A0		D15		PC6		ADC10
	5,	// A1		D16		PB2		ADC5
	2,	// A2		D17		PD5		ADC2
	3,	// A3		D18		PD6		ADC3
	9,	// A4		D19		PC5		ADC9 / AMP1+
	8,	// A5		D20		PC4		ADC8 / AMP1-
	1,	// A6		D0		PD4		ADC1
	6,	// A7		D2		PB5		ADC6 / AMP2-
	7,	// A8		D5		PB6		ADC7
	4,	// A9		D13		PB7		ADC4
	11,	// A10		--		--		Internal core temp sensor
	12,	// A11		--		--		Internal Vcc/4
	14,	// AD0		--		--		Differential AMP0
	15,	// AD1		--		--		Differential AMP1
	16	// AD2		--		--		Differential AMP2
};

#endif
// These serial port names are intended to allow libraries and architecture-neutral
// sketches to automatically default to the correct port name for a particular type
// of use.  For example, a GPS module would normally connect to SERIAL_PORT_HARDWARE_OPEN,
// the first hardware serial port whose RX/TX pins are not dedicated to another use.
//
// SERIAL_PORT_MONITOR        Port which normally prints to the Arduino Serial Monitor
//
// SERIAL_PORT_USBVIRTUAL     Port which is USB virtual serial
//
// SERIAL_PORT_LINUXBRIDGE    Port which connects to a Linux system via Bridge library
//
// SERIAL_PORT_HARDWARE       Hardware serial port, physical RX & TX pins.
//
// SERIAL_PORT_HARDWARE_OPEN  Hardware serial ports which are open for use.  Their RX & TX
//                            pins are NOT connected to anything by default.
#define SERIAL_PORT_MONITOR   Serial
#define SERIAL_PORT_HARDWARE  Serial

#endif
