/**********************************************************/
/* Optiboot bootloader for Arduino                        */
/*                                                        */
/* http://optiboot.googlecode.com                         */
/*                                                        */
/* Arduino-maintained version : See README.TXT            */
/* http://code.google.com/p/arduino/                      */
/*                                                        */
/* Heavily optimised bootloader that is faster and        */
/* smaller than the Arduino standard bootloader           */
/*                                                        */
/* Enhancements:                                          */
/*   Fits in 512 bytes, saving 1.5K of code space         */
/*   Background page erasing speeds up programming        */
/*   Higher baud rate speeds up programming               */
/*   Written almost entirely in C                         */
/*   Customisable timeout with accurate timeconstant      */
/*   Optional virtual UART. No hardware UART required.    */
/*   Optional virtual boot partition for devices without. */
/*                                                        */
/* What you lose:                                         */
/*   Implements a skeleton STK500 protocol which is       */
/*     missing several features including EEPROM          */
/*     programming and non-page-aligned writes            */
/*   High baud rate breaks compatibility with standard    */
/*     Arduino flash settings                             */
/*                                                        */
/* Fully supported:                                       */
/*   ATmega168 based devices  (Diecimila etc)             */
/*   ATmega328P based devices (Duemilanove etc)           */
/*                                                        */
/* Alpha test                                             */
/*   ATmega1280 based devices (Arduino Mega)              */
/*                                                        */
/* Work in progress:                                      */
/*   ATmega644P based devices (Sanguino)                  */
/*   ATtiny84 based devices (Luminet)                     */
/*                                                        */
/* Does not support:                                      */
/*   USB based devices (eg. Teensy)                       */
/*                                                        */
/* Assumptions:                                           */
/*   The code makes several assumptions that reduce the   */
/*   code size. They are all true after a hardware reset, */
/*   but may not be true if the bootloader is called by   */
/*   other means or on other hardware.                    */
/*     No interrupts can occur                            */
/*     UART and Timer 1 are set to their reset state      */
/*     SP points to RAMEND                                */
/*                                                        */
/* Code builds on code, libraries and optimisations from: */
/*   stk500boot.c          by Jason P. Kyle               */
/*   Arduino bootloader    http://www.arduino.cc          */
/*   Spiff's 1K bootloader http://spiffie.org/know/arduino_1k_bootloader/bootloader.shtml */
/*   avr-libc project      http://nongnu.org/avr-libc     */
/*   Adaboot               http://www.ladyada.net/library/arduino/bootloader.html */
/*   AVR305                Atmel Application Note         */
/*                                                        */
/* This program is free software; you can redistribute it */
/* and/or modify it under the terms of the GNU General    */
/* Public License as published by the Free Software       */
/* Foundation; either version 2 of the License, or        */
/* (at your option) any later version.                    */
/*                                                        */
/* This program is distributed in the hope that it will   */
/* be useful, but WITHOUT ANY WARRANTY; without even the  */
/* implied warranty of MERCHANTABILITY or FITNESS FOR A   */
/* PARTICULAR PURPOSE.  See the GNU General Public        */
/* License for more details.                              */
/*                                                        */
/* You should have received a copy of the GNU General     */
/* Public License along with this program; if not, write  */
/* to the Free Software Foundation, Inc.,                 */
/* 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA */
/*                                                        */
/* Licence can be viewed at                               */
/* http://www.fsf.org/licenses/gpl.txt                    */
/*                                                        */
/**********************************************************/


/**********************************************************/
/*                                                        */
/* Optional defines:                                      */
/*                                                        */
/**********************************************************/
/*                                                        */
/* BIG_BOOT:                                              */
/* Build a 1k bootloader, not 512 bytes. This turns on    */
/* extra functionality.                                   */
/*                                                        */
/* BAUD_RATE:                                             */
/* Set bootloader baud rate.                              */
/*                                                        */
/* LUDICROUS_SPEED:                                       */
/* 230400 baud :-)                                        */
/*                                                        */
/* SOFT_UART:                                             */
/* Use AVR305 soft-UART instead of hardware UART.         */
/*                                                        */
/* LED_START_FLASHES:                                     */
/* Number of LED flashes on bootup.                       */
/*                                                        */
/* LED_DATA_FLASH:                                        */
/* Flash LED when transferring data. For boards without   */
/* TX or RX LEDs, or for people who like blinky lights.   */
/*                                                        */
/* SUPPORT_EEPROM:                                        */
/* Support reading and writing from EEPROM. This is not   */
/* used by Arduino, so off by default.                    */
/*                                                        */
/* TIMEOUT_MS:                                            */
/* Bootloader timeout period, in milliseconds.            */
/* 500,1000,2000,4000,8000 supported.                     */
/*                                                        */
/**********************************************************/

/**********************************************************/
/* Version Numbers!                                       */
/*                                                        */
/* Arduino Optiboot now includes this Version number in   */
/* the source and object code.                            */
/*                                                        */
/* Version 3 was released as zip from the optiboot        */
/*  repository and was distributed with Arduino 0022.     */
/* Version 4 starts with the arduino repository commit    */
/*  that brought the arduino repository up-to-date with   */
/* the optiboot source tree changes since v3.             */
/*                                                        */
/**********************************************************/

/**********************************************************/
/* Edit History:					  */
/*							  */
/* 4.4 WestfW: add initialization of address to keep      */
/*             the compiler happy.  Change SC'ed targets. */
/*             Return the SW version via READ PARAM       */
/* 4.3 WestfW: catch framing errors in getch(), so that   */
/*             AVRISP works without HW kludges.           */
/*  http://code.google.com/p/arduino/issues/detail?id=368n*/
/* 4.2 WestfW: reduce code size, fix timeouts, change     */
/*             verifySpace to use WDT instead of appstart */
/* 4.1 WestfW: put version number in binary.		  */
/**********************************************************/

#define OPTIBOOT_MAJVER 4
#define OPTIBOOT_MINVER 4

#define MAKESTR(a) #a
#define MAKEVER(a, b) MAKESTR(a*256+b)

asm("  .section .version\n"
    "optiboot_version:  .word " MAKEVER(OPTIBOOT_MAJVER, OPTIBOOT_MINVER) "\n"
    "  .section .text\n");

#include <inttypes.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

// <avr/boot.h> uses sts instructions, but this version uses out instructions
// This saves cycles and program memory.
#include "boot.h"


// We don't use <avr/wdt.h> as those routines have interrupt overhead we don't need.

#include "pin_defs.h"
#include "stk500.h"

#ifndef LED_START_FLASHES
#define LED_START_FLASHES 0
#endif

#ifdef LUDICROUS_SPEED
#define BAUD_RATE 230400L
#endif

/* set the UART baud rate defaults */
#ifndef BAUD_RATE
#if F_CPU >= 8000000L
#define BAUD_RATE   115200L // Highest rate Avrdude win32 will support
#elsif F_CPU >= 1000000L
#define BAUD_RATE   9600L   // 19200 also supported, but with significant error
#elsif F_CPU >= 128000L
#define BAUD_RATE   4800L   // Good for 128kHz internal RC
#else
#define BAUD_RATE 1200L     // Good even at 32768Hz
#endif
#endif

/* Switch in soft UART for hard baud rates */
#if (F_CPU/BAUD_RATE) > 280 // > 57600 for 16MHz
#ifndef SOFT_UART
#define SOFT_UART
#endif
#endif

/* Watchdog settings */
#define WATCHDOG_OFF    (0)
#define WATCHDOG_16MS   (_BV(WDE))
#define WATCHDOG_32MS   (_BV(WDP0) | _BV(WDE))
#define WATCHDOG_64MS   (_BV(WDP1) | _BV(WDE))
#define WATCHDOG_125MS  (_BV(WDP1) | _BV(WDP0) | _BV(WDE))
#define WATCHDOG_250MS  (_BV(WDP2) | _BV(WDE))
#define WATCHDOG_500MS  (_BV(WDP2) | _BV(WDP0) | _BV(WDE))
#define WATCHDOG_1S     (_BV(WDP2) | _BV(WDP1) | _BV(WDE))
#define WATCHDOG_2S     (_BV(WDP2) | _BV(WDP1) | _BV(WDP0) | _BV(WDE))
#define WATCHDOG_4S     (_BV(WDP3) | _BV(WDE))
#define WATCHDOG_8S     (_BV(WDP3) | _BV(WDP0) | _BV(WDE))

/* Function Prototypes */
/* The main function is in init9, which removes the interrupt vector table */
/* we don't need. It is also 'naked', which means the compiler does not    */
/* generate any entry or exit code itself. */
int main(void) __attribute__ ((naked)) __attribute__ ((section (".init9")));
void putch(char);
uint8_t getch(void);
static inline void getNch(uint8_t); /* "static inline" is a compiler hint to reduce code size */
void verifySpace();
static inline void flash_led(uint8_t);
uint8_t getLen();
static inline void watchdogReset();
void watchdogConfig(uint8_t x);
void appStart() __attribute__ ((naked));

//new definitions for ARC related MCUs
#if defined(__AVR_ATmega32M1__)
//#define RAMSTART (0x100)
#define NRWWSTART (0x7000)
#elif defined(__AVR_ATmega64M1__)
//#define RAMSTART (0x100)
#define NRWWSTART (0xE000)
#endif

/* C zero initialises all global variables. However, that requires */
/* These definitions are NOT zero initialised, but that doesn't matter */
/* This allows us to drop the zero init code, saving us memory */
#define buff    ((uint8_t*)(RAMSTART))

/* main program starts here */
int main(void) {
	uint8_t ch;

	/*
	 * Making these local and in registers prevents the need for initializing
	 * them, and also saves space because code no longer stores to memory.
	 * (initializing address keeps the compiler happy, but isn't really
	 *  necessary, and uses 4 bytes of flash.)
	 */
	register uint16_t address = 0;
	register uint8_t  length;

	// After the zero init loop, this is the first code to run.
	//
	// This code makes the following assumptions:
	//  No interrupts will execute
	//  SP points to RAMEND
	//  r1 contains zero
	//
	// If not, uncomment the following instructions:
	// cli();
	asm volatile ("clr __zero_reg__");

	// Adaboot no-wait mod
	ch = MCUSR;
	MCUSR = 0;
	if (!(ch & _BV(EXTRF))) {
		appStart();
	}

#if LED_START_FLASHES > 0
	// Set up Timer 1 for timeout counter
	TCCR1B = _BV(CS12) | _BV(CS10); // div 1024
#endif

	// *** Changes by CODINGHEAD
	// This modification sets up the LIN capable UART on the mega32C1 device
	// which is substantially different to the UART on other mega MCUs

	// Sets USART mode and enables full-duplex communication
	// Disable re-sync; 8-time bit sampling mode

	LINBTR = _BV(LDISR) | 8;
	// Uses LINBTR LBTR[5:0] of 8 to calcualte baud rate
	LINBRR = (uint16_t) ( F_CPU / (8 * BAUD_RATE)) - 1;
	// Enable the LIN module; enable Rx and Tx side of LIN in UART mode
	LINCR  = _BV(LENA) | _BV(LCMD2) | _BV(LCMD1) | _BV(LCMD0);

	// Set up watchdog to trigger after 500ms
	watchdogConfig(WATCHDOG_1S);

	/* Set LED pin as output */
	LED_DDR |= _BV(LED);


	#if LED_START_FLASHES > 0
	/* Flash onboard LED to signal entering of bootloader */
	flash_led(LED_START_FLASHES * 2);
	#endif

	/* Forever loop */
	for (;;) {
		/* get character from UART */
		ch = getch();

		if(ch == STK_GET_PARAMETER) {
			unsigned char which = getch();
			verifySpace();
			if (which == 0x82) {
			/*
			* Send optiboot version as "minor SW version"
			*/
				putch(OPTIBOOT_MINVER);
			} else if (which == 0x81) {
				putch(OPTIBOOT_MAJVER);
			} else {
				/*
				* GET PARAMETER returns a generic 0x03 reply for
				* other parameters - enough to keep Avrdude happy
				*/
				putch(0x03);
			}
		}
		else if(ch == STK_SET_DEVICE) {
			// SET DEVICE is ignored
			getNch(20);
		}
		else if(ch == STK_SET_DEVICE_EXT) {
			// SET DEVICE EXT is ignored
			getNch(5);
		}
		else if(ch == STK_LOAD_ADDRESS) {
			// LOAD ADDRESS
			uint16_t newAddress;
			newAddress = getch();
			newAddress = (newAddress & 0xff) | (getch() << 8);
#ifdef RAMPZ
			// Transfer top bit to RAMPZ
			RAMPZ = (newAddress & 0x8000) ? 1 : 0;
#endif
			newAddress += newAddress; // Convert from word address to byte address
			address = newAddress;
			verifySpace();
		}
		else if(ch == STK_UNIVERSAL) {
			// UNIVERSAL command is ignored
			getNch(4);
			putch(0x00);
		}
		/* Write memory, length is big endian and is in bytes */
		else if(ch == STK_PROG_PAGE) {
			// PROGRAM PAGE - we support flash programming only, not EEPROM
			uint8_t *bufPtr;
			uint16_t addrPtr;

			getch();			/* getlen() */
			length = getch();
			getch();

			// If we are in RWW section, immediately start page erase
			if (address < NRWWSTART) __boot_page_erase_short((uint16_t)(void*)address);

			// While that is going on, read in page contents
			bufPtr = buff;
			do *bufPtr++ = getch();
			while (--length);

			// If we are in NRWW section, page erase has to be delayed until now.
			// Todo: Take RAMPZ into account
			if (address >= NRWWSTART) __boot_page_erase_short((uint16_t)(void*)address);

			// Read command terminator, start reply
			verifySpace();

			// If only a partial page is to be programmed, the erase might not be complete.
			// So check that here
			boot_spm_busy_wait();

			// Copy buffer into programming buffer
			bufPtr = buff;
			addrPtr = (uint16_t)(void*)address;
			ch = SPM_PAGESIZE / 2;
			do {
				uint16_t a;
				a = *bufPtr++;
				a |= (*bufPtr++) << 8;
				__boot_page_fill_short((uint16_t)(void*)addrPtr,a);
				addrPtr += 2;
			} while (--ch);

			// Write from programming buffer
			__boot_page_write_short((uint16_t)(void*)address);
			boot_spm_busy_wait();

#if defined(RWWSRE)
			// Reenable read access to flash
			boot_rww_enable();
#endif

		}
		/* Read memory block mode, length is big endian.  */
		else if(ch == STK_READ_PAGE) {
			// READ PAGE - we only read flash
			getch();			/* getlen() */
			length = getch();
			getch();

			verifySpace();

			do putch(pgm_read_byte_near(address++));
			while (--length);
		}

		/* Get device signature bytes  */
		else if(ch == STK_READ_SIGN) {
			// READ SIGN - return what Avrdude wants to hear
			verifySpace();
			putch(SIGNATURE_0);
			putch(SIGNATURE_1);
			putch(SIGNATURE_2);
		}
		else if (ch == 'Q') {
			// Adaboot no-wait mod
			watchdogConfig(WATCHDOG_16MS);
			verifySpace();
		}
		else {
			// This covers the response to commands like STK_ENTER_PROGMODE
			verifySpace();
			//putch(ch);
		}
		putch(STK_OK);
	}
}

void putch(char ch) {
	while (LINSIR & _BV(LBUSY))
		;
	LINDAT = ch;
}

uint8_t getch(void) {
	LED_PIN |= _BV(LED);
	// LRXOK is 1 when a byte has been received
	while ((LINSIR & _BV(LRXOK)) == 0)
		;

	// LERR or's together all of the possible error bits from the LINERR
	// register. If it is 1, there was some sort of error (framing, overrun...)
	if (!(LINSIR & _BV(LERR))) {
		watchdogReset();
	}
	LED_PIN |= _BV(LED);
	return LINDAT;
}

void getNch(uint8_t count) {
	do getch(); while (--count);
	verifySpace();
}

void verifySpace() {
	if (getch() != CRC_EOP) {
		watchdogConfig(WATCHDOG_16MS);	// shorten WD timeout
		while (1) {						// and busy-loop so that WD causes
			;							//  a reset and app start.
		}
	}
	putch(STK_INSYNC);
}

#if LED_START_FLASHES > 0
void flash_led(uint8_t count) {
	do {
		TCNT1 = -(F_CPU/(1024*16));
		TIFR1 = _BV(TOV1);
		while(!(TIFR1 & _BV(TOV1))) {
			;
		}
		LED_PIN |= _BV(LED);
		watchdogReset();
	} while (--count);
}
#endif

// Watchdog functions. These are only safe with interrupts turned off.
void watchdogReset() {
	__asm__ __volatile__ (
		"wdr\n"
	);
}

void watchdogConfig(uint8_t x) {
	WDTCSR = _BV(WDCE) | _BV(WDE);
	WDTCSR = x;
}

void appStart() {
	watchdogConfig(WATCHDOG_OFF);

	//TODO:turn LIN/UART off

	__asm__ __volatile__ (
		// Jump to RST vector
		"clr r30\n"
		"clr r31\n"
		"ijmp\n"
	);
}
