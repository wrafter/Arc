/*
  HardwareSerial_private.h - Hardware serial library for Wiring
  Copyright (c) 2006 Nicholas Zambetti.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  Modified 23 November 2006 by David A. Mellis
  Modified 28 September 2010 by Mark Sproul
  Modified 14 August 2012 by Alarus

  Modified to support ATmega32M1, ATmega64M1, etc.   Mar 2016
    based on work from CODINGHEAD (Stuart Cording)
        Al Thomason:   https://github.com/thomasonw/ATmegaxxM1-C1
                       http://smartmppt.blogspot.com/search/label/xxM1-IDE


*/

#include "wiring_private.h"

// this next line disables the entire HardwareSerial.cpp,
// this is so I can support Attiny series and any other chip without a uart
// MCU known as ATmega M1 family
#if defined(HAVE_HWSERIAL0)

// Ensure that the various bit positions we use are available with a 0
// postfix, so we can always use the values for UART0 for all UARTs. The
// alternative, passing the various values for each UART to the
// HardwareSerial constructor also works, but makes the code bigger and
// slower.
#if !defined(TXC0)
//#if defined(LINBRRH)
// know case for ATmega32/64M1
  #define TXC0 LTXOK
  #define RXEN0 LCMD1
  #define TXEN0 LCMD0
  #define RXCIE0 LENRXOK
  #define UDRIE0 LENTXOK
// #define U2X0 zxcRU2X
  #define UPE0 LPERR
  #define UDRE0 LTXOK
#else
  #error No UART found in HardwareSerial.cpp
//#endif
#endif // !defined TXC0

// Constructors ////////////////////////////////////////////////////////////////

HardwareSerial::HardwareSerial(
  volatile uint8_t *linbrrh, volatile uint8_t *linbrrl,
  volatile uint8_t *linsir, volatile uint8_t *linenir,
  volatile uint8_t *lincr, volatile uint8_t *lindat) :
	_ubrrh(linbrrh), _ubrrl(linbrrl),
	_ucsra(linsir), _ucsrb(linenir), _ucsrc(lincr),
	_udr(lindat),
	_rx_buffer_head(0), _rx_buffer_tail(0),
	_tx_buffer_head(0), _tx_buffer_tail(0)
	// Note that with the LIN, the Rx function handler will be called from the Tx IRQ handler, as
	// there is only one interupt vector.
{
}

// Actual interrupt handlers //////////////////////////////////////////////////////////////

void HardwareSerial::_rx_complete_irq(void)
{
	// Rx AND Tx interrupts are handled together here for the LIN Module in UART mode.
	// So we need to figure out which one caused the IRQ and handle them both here..
	if (bit_is_set(LINSIR, LRXOK)) {		// Is there also an Rx character to handle?
		unsigned char c = *_udr;
		rx_buffer_index_t i = (unsigned int)(_rx_buffer_head + 1) % SERIAL_RX_BUFFER_SIZE;

		// if we should be storing the received character into the location
		// just before the tail (meaning that the head would advance to the
		// current location of the tail), we're about to overflow the buffer
		// and so we don't write the character or advance the head.
		if (i != _rx_buffer_tail) {
		_rx_buffer[_rx_buffer_head] = c;
		_rx_buffer_head = i;
		}
	}

	if (bit_is_set(LINSIR, LTXOK)) {			// Now, lets check to see if we arrived here because of a TX interrupt?
		if (_tx_buffer_head != _tx_buffer_tail)	// Yes we did, and there are some characters in the buffer to send out.
				_tx_udr_empty_irq();				// Go send them.
		else {
			cbi(*_ucsrb, UDRIE0);				// Buffer empty, so disable interrupts
			sbi(*_ucsra, TXC0);					// And clear this Tx flag that brought us here
		}										// Need to do this check here, as _tx_udr_empty_irq() does not
	}											// make the check before trying to send another character out...
}

#endif // whole file
