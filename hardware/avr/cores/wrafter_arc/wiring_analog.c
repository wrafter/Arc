/*
  wiring_analog.c - analog input and output
  Part of Arduino - http://www.arduino.cc/

  Copyright (c) 2005-2006 David A. Mellis

  Modified to support ATmega32M1, ATmega64M1, etc.   Mar 2016
        Al Thomason:   https://github.com/thomasonw/ATmegaxxM1-C1
                       http://smartmppt.blogspot.com/search/label/xxM1-IDE

	Enabled analogWrite() to support DAC
	Allow Analog read channel > 8
	  including internal temp sensor, and differential ADC channels (See pins.h in variants)



  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General
  Public License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place, Suite 330,
  Boston, MA  02111-1307  USA

  Modified 28 September 2010 by Mark Sproul
*/

#include "wiring_private.h"
#include "pins_arduino.h"

uint8_t analog_reference = DEFAULT;

void analogReference(uint8_t mode) {
	// can't actually set the register here because the default setting
	// will connect AVCC and the AREF pin, which would cause a short if
	// there's something connected to AREF.
	analog_reference = mode;
}

int analogRead(uint8_t pin) {
	uint8_t low, high;

	if (pin == AD0) {
		sbi(AMP0CSR, AMP0EN); 	// ensure AMP0 has been started
	}
	if (pin == AD1) {
		sbi(AMP1CSR, AMP1EN); 	// ensure AMP1 has been started
	}
	if (pin == AD2) {
		sbi(AMP2CSR, AMP2EN); 	// ensure AMP2 has been started
	}

	if (pin >= 18) {
		pin -= 18;	// allow for channel or pin numbers.  (Allows access to channels we have not defined)
	}
	if (pin >= 18) {
		return(0);	// Do not attempt to switch mux to reserved channels, seems to wedge chip...
	}

	pin = analogPinToChannel(pin);
	if (pin >= 14) {
		pin -= 14; // allow for channel or pin numbers
	}

	// set the analog reference (high two bits of ADMUX) and select the
	// channel (low 4 bits).  this also sets ADLAR (left-adjust result)
	// to 0 (the default).
	ADMUX = (analog_reference << REFS0) | (pin & 0x1F); // 5-bit mask, allows for > 8 channels, including differential channels.

	// without a delay, we seem to read from the wrong channel
	//delay(1);

	// start the conversion
	sbi(ADCSRA, ADSC);

	// ADSC is cleared when the conversion finishes
	while (bit_is_set(ADCSRA, ADSC));

	// we have to read ADCL first; doing so locks both ADCL
	// and ADCH until ADCH is read.  reading ADCL second would
	// cause the results of each conversion to be discarded,
	// as ADCL and ADCH would be locked when it completed.
	low  = ADCL;
	high = ADCH;

    // One more special thing we need to check for, was this a differential amp ADC?
	// FIXME: if temperature is being read, two converions are needed
	int16_t sum = (high << 8) | low;

	if ((pin >= 14) & (pin <= 16) & (sum > 0x01FF)) {	// (pin was transformed above by analogPinToChannel())
		sum   -= 0x3FF;									// Diff amps return an 10-bit + or - value, offset by 512.
	}													// If we received a value larger than that, it really is
	return(sum);										// a negative value (meaning, '-' input has a higher voltage the '+'
}

// Right now, PWM output only works on the pins with
// hardware support.  These are defined in the appropriate
// pins_*.c file.  For the rest of the pins, we default
// to digital output.
void analogWrite(uint8_t pin, int val) {
// Addition for DAC
	if (pin == DAC_PORT) { // Forces the port to an OUTPUT and then does DAC conversion.
		if (!(DACON & _BV(DAOE))) {
			pinMode(pin, OUTPUT);
			DACON = ((1<<DAEN) | (1<<DAOE));	// Enable DAC, enable its port as output.
		}
		DACL = val;							// Send the value out to the DAC (lower 8-bits).
		DACH = (val >> 8) & 0x03;			// Writing to DACH causes the DAC to do its conversion.
		return;
	} else if (pin == DAC_INTERNAL) {
		if (!(DACON & _BV(DAEN)) || DACON & (1<<DAOE)) {
			DACON = (1<<DAEN);				// Enable DAC, keep result for internal use
		}
		DACL = val;							// Send the value out to the DAC (lower 8-bits).
		DACH = (val >> 8) & 0x03;			// Writing to DACH causes the DAC to do its conversion.
		return;
	}

	// We need to make sure the PWM output is enabled for those pins
	// that support it, as we turn it off when digitally reading or
	// writing with them.  Also, make sure the pin is in output mode
	// for consistenty with Wiring, which doesn't require a pinMode
	// call for the analog output pins.
	pinMode(pin, OUTPUT);
	if (val == 0) {
		digitalWrite(pin, LOW);
	} else if (val == 255) {
		digitalWrite(pin, HIGH);
	}else {
		switch(digitalPinToTimer(pin)) {
		case TIMER0A:
			// connect pwm to pin on timer 0, channel A
			sbi(TCCR0A, COM0A1);
			OCR0A = val; // set pwm duty
			break;
		case TIMER1A:
			// connect pwm to pin on timer 1, channel A
			sbi(TCCR1A, COM1A1);
			OCR1A = val; // set pwm duty
			break;
		case TIMER1B:
			// connect pwm to pin on timer 1, channel B
			sbi(TCCR1A, COM1B1);
			OCR1B = val; // set pwm duty
			break;
		case NOT_ON_TIMER:
		default:
			if (val < 128) {
				digitalWrite(pin, LOW);
			} else {
				digitalWrite(pin, HIGH);
			}
		}
	}
}

