/* Tone.cpp

  A Tone Generator Library

  Written by Brett Hagman

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

Version Modified By Date     Comments
------- ----------- -------- --------
0001    B Hagman    09/08/02 Initial coding
0002    B Hagman    09/08/18 Multiple pins
0003    B Hagman    09/08/18 Moved initialization from constructor to begin()
0004    B Hagman    09/09/26 Fixed problems with ATmega8
0005    B Hagman    09/11/23 Scanned prescalars for best fit on 8 bit timers
                    09/11/25 Changed pin toggle method to XOR
                    09/11/25 Fixed timer0 from being excluded
0006    D Mellis    09/12/29 Replaced objects with functions
0007    M Sproul    10/08/29 Changed #ifdefs from cpu to register
0008    S Kanemoto  12/06/22 Fixed for Leonardo by @maris_HY
0009    J Reucker   15/04/10 Issue #292 Fixed problems with ATmega8 (thanks to Pete62)
0010    jipp        15/04/13 added additional define check #2923
*************************************************/

#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "Arduino.h"
#include "pins_arduino.h"

volatile long timer0_toggle_count;
volatile uint8_t *timer0_pin_port;
volatile uint8_t timer0_pin_mask;

volatile long timer1_toggle_count;
volatile uint8_t *timer1_pin_port;
volatile uint8_t timer1_pin_mask;


#define AVAILABLE_TONE_PINS 1
#define USE_TIMER1

const uint8_t PROGMEM tone_pin_to_timer_PGM[] = { 1 /*, 1 */ };
static uint8_t tone_pins[AVAILABLE_TONE_PINS] = { 255 /*, 255 */ };


static int8_t toneBegin(uint8_t _pin) {
	int8_t _timer = -1;

	// if we're already using the pin, the timer should be configured.
	for (int i = 0; i < AVAILABLE_TONE_PINS; i++) {
		if (tone_pins[i] == _pin) {
			return pgm_read_byte(tone_pin_to_timer_PGM + i);
		}
	}

	// search for an unused timer.
	for (int i = 0; i < AVAILABLE_TONE_PINS; i++) {
		if (tone_pins[i] == 255) {
			tone_pins[i] = _pin;
			_timer = pgm_read_byte(tone_pin_to_timer_PGM + i);
			break;
		}
	}

	if (_timer != -1) {
		// Set timer specific stuff
		// All timers in CTC mode
		// 8 bit timers will require changing prescalar values,
		// whereas 16 bit timers are set to either ck/1 or ck/64 prescalar
		switch (_timer) {
		case 0:
			// 8 bit timer
			TCCR0A = 0;
			TCCR0B = 0;
			bitWrite(TCCR0A, WGM01, 1);
			bitWrite(TCCR0B, CS00, 1);
			timer0_pin_port = portOutputRegister(digitalPinToPort(_pin));
			timer0_pin_mask = digitalPinToBitMask(_pin);
			break;
		case 1:
			// 16 bit timer
			TCCR1A = 0;
			TCCR1B = 0;
			bitWrite(TCCR1B, WGM12, 1);
			bitWrite(TCCR1B, CS10, 1);
			timer1_pin_port = portOutputRegister(digitalPinToPort(_pin));
			timer1_pin_mask = digitalPinToBitMask(_pin);
			break;
		}
	}
	return _timer;
}

// frequency (in hertz) and duration (in milliseconds).
void tone(uint8_t _pin, unsigned int frequency, unsigned long duration) {
	uint8_t prescalarbits = 0b001;
	long toggle_count = 0;
	uint32_t ocr = 0;
	int8_t _timer;

	_timer = toneBegin(_pin);

	if (_timer >= 0) {
		// Set the pinMode as OUTPUT
		pinMode(_pin, OUTPUT);

		// if we are using an 8 bit timer, scan through prescalars to find the best fit
		if (_timer == 0) {
			ocr = F_CPU / frequency / 2 - 1;
			prescalarbits = 0b001;  // ck/1: same for both timers
			if (ocr > 255) {
				ocr = F_CPU / frequency / 2 / 8 - 1;
				prescalarbits = 0b010;  // ck/8: same for both timers

				if (ocr > 255) {
					ocr = F_CPU / frequency / 2 / 64 - 1;
					prescalarbits =  0b011;

					if (ocr > 255) {
						ocr = F_CPU / frequency / 2 / 256 - 1;
						prescalarbits =  0b100;
						if (ocr > 255) {
							// can't do any better than /1024
							ocr = F_CPU / frequency / 2 / 1024 - 1;
							prescalarbits = 0b101;
						}
					}
				}
			}
			TCCR0B = (TCCR0B & 0b11111000) | prescalarbits;
		} else {
			// two choices for the 16 bit timers: ck/1 or ck/64
			ocr = F_CPU / frequency / 2 - 1;

			prescalarbits = 0b001;
			if (ocr > 0xffff) {
				ocr = F_CPU / frequency / 2 / 64 - 1;
				prescalarbits = 0b011;
			}

			TCCR1B = (TCCR1B & 0b11111000) | prescalarbits;
		}

		// Calculate the toggle count
		if (duration > 0) {
			toggle_count = 2 * frequency * duration / 1000;
		} else {
			toggle_count = -1;
		}

		// Set the OCR for the given timer,
		// set the toggle count,
		// then turn on the interrupts
		switch (_timer) {
		case 0:
			OCR0A = ocr;
			timer0_toggle_count = toggle_count;
			bitWrite(TIMSK0, OCIE0A, 1);
			break;
		case 1:
			OCR1A = ocr;
			timer1_toggle_count = toggle_count;
			bitWrite(TIMSK1, OCIE1A, 1);
			break;
		}
	}
}

// XXX: this function only works properly for timer 2 (the only one we use
// currently).  for the others, it should end the tone, but won't restore
// proper PWM functionality for the timer.
void disableTimer(uint8_t _timer) {
	switch (_timer) {
	case 0:
		TIMSK0 = 0;
		break;
	case 1:
		bitWrite(TIMSK1, OCIE1A, 0);
		break;
	}
}

void noTone(uint8_t _pin) {
	int8_t _timer = -1;

	for (int i = 0; i < AVAILABLE_TONE_PINS; i++) {
		if (tone_pins[i] == _pin) {
			_timer = pgm_read_byte(tone_pin_to_timer_PGM + i);
			tone_pins[i] = 255;
			break;
		}
	}

	disableTimer(_timer);

	digitalWrite(_pin, 0);
}

#ifdef USE_TIMER0
ISR(TIMER0_COMPA_vect)
{
  if (timer0_toggle_count != 0)
  {
    // toggle the pin
    *timer0_pin_port ^= timer0_pin_mask;

    if (timer0_toggle_count > 0)
      timer0_toggle_count--;
  }
  else
  {
    disableTimer(0);
    *timer0_pin_port &= ~(timer0_pin_mask);  // keep pin low after stop
  }
}
#endif


#ifdef USE_TIMER1
ISR(TIMER1_COMPA_vect)
{
  if (timer1_toggle_count != 0)
  {
    // toggle the pin
    *timer1_pin_port ^= timer1_pin_mask;

    if (timer1_toggle_count > 0)
      timer1_toggle_count--;
  }
  else
  {
    disableTimer(1);
    *timer1_pin_port &= ~(timer1_pin_mask);  // keep pin low after stop
  }
}
#endif
