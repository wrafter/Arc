/*
 * CAN.h
 *
 * This file is part of the Wrafter ARC CAN bus library.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: David Bengtsson
 */

#ifndef CAN_H
#define CAN_H

#include "CANframe.h"

extern const uint8_t CAN_BFFR_FULL;     /* CAN buffer full constant */

/************************************************************************/
/* CAN main class.                                                      */
/************************************************************************/
class CANb
{
        private:
        uint8_t nbr_of_rx_mobs;         /* Number of RX MObs */
        uint8_t rx_mob_cnt;             /* Number of assigned RX MObs */

        public:
        CANb(void);

        CAN_RES begin(void);
        CAN_RES begin(CAN_BAUD);
        CAN_RES begin(CAN_BAUD, uint8_t);
        CAN_RES begin(CAN_BAUD, uint8_t, CAN_MODE);

        void attachInterrupt(CAN_LINE, voidFuncPtr);
        CAN_RES setRxMask(uint32_t, uint32_t);
        CAN_RES setRxMask(uint32_t, uint32_t, CAN_FORMAT);

        uint8_t available(void);
        uint8_t getErrorCnt(CAN_LINE);
        uint32_t getId(void);
        CAN_RES read(CANframe &);
        CAN_RES write(CANframe &);
};

extern CANb CAN;        /* CAN bus object adapter */

#endif
