/*
 * CANhardw.h
 *
 * This file is part of the Wrafter ARC CAN bus library.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: David Bengtsson
 */

#ifndef CANHARDW_H
#define CANHARDW_H

#include "../CANframe.h"

extern const uint8_t NBR_OF_MOBS;       /* Number of hardware specific MObs */

/************************************************************************/
/* Global function declarations.                                        */
/************************************************************************/
extern voidFuncPtr rx_isr_ptr;          /* Interrupt RX callback function */
extern voidFuncPtr tx_isr_ptr;          /* Interrupt TX callback function */

namespace CANhardw
{
        void init(uint8_t, uint8_t);
        void setupRx(uint8_t, uint32_t, uint32_t, CAN_FORMAT);
        CAN_RES setupTx(uint8_t, CANframe &);
        uint8_t getErrors(CAN_LINE);
}

#endif
