/*
 * conf.h
 *
 * This file is part of the Wrafter ARC CAN bus library.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: David Bengtsson
 */

#ifndef CONF_H
#define CONF_H

#include <inttypes.h>

/************************************************************************/
/* Global constant declarations.                                        */
/************************************************************************/
enum CAN_BAUD {
        CAN_BAUD_100K,
        CAN_BAUD_125K,
        CAN_BAUD_200K,
        CAN_BAUD_250K,
        CAN_BAUD_500K,
        CAN_BAUD_1000K
};

enum CAN_FORMAT {
        CAN_FORMAT_STD,
        CAN_FORMAT_EXT
};

enum CAN_FRAME {
        CAN_FRAME_DAT,
        CAN_FRAME_REQ
};

enum CAN_LINE {
        CAN_LINE_RX,
        CAN_LINE_TX
};

enum CAN_MODE {
        CAN_MODE_ENABLE,
        CAN_MODE_LISTEN,
        CAN_MODE_TTC,
        CAN_MODE_STNDBY
};

enum CAN_RES {
        CAN_OK,         // 0
        CAN_BUSY,       // 1
        CAN_LIM_RCHD,   // 2
        CAN_NO_MSG,     // 3
        CAN_VAL_OOR,    // 4
        CAN_NO_ID = UINT32_MAX
};

#endif
