/*
 * CANframe.cpp
 *
 * This file is part of the Wrafter ARC CAN bus library.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: David Bengtsson
 */

#include <Arduino.h>
#include "CANframe.h"

const uint16_t MAX_ID_STD = 0x7FF;      /* Standard format max ID */
const uint32_t MAX_ID_EXT = 0x1FFFFFFF; /* Extended format max ID */
const uint8_t NBR_OF_BYTES = 8;         /* Allowed number of bytes */

/************************************************************************/
/* CANframe - Class constructor.                                        */
/************************************************************************/
CANframe::CANframe(void)
{
        CANframe::init(UINT32_MAX, 0, NULL, CAN_FORMAT_EXT, CAN_FRAME_DAT);
}

/************************************************************************/
/* CANframe - Class constructor.                                        */
/*                                                                      */
/* @param id: Frame ID.                                                 */
/************************************************************************/
CANframe::CANframe(uint32_t id)
{
        id = CANframe::checkId(id, CAN_FORMAT_EXT);
        CANframe::init(id, 0, NULL, CAN_FORMAT_EXT, CAN_FRAME_DAT);
}

/************************************************************************/
/* CANframe - Class constructor.                                        */
/*                                                                      */
/* @param id: Frame ID.                                                 */
/* @param dlc: Frame data length.                                       */
/* @param data: Frame data.                                             */
/************************************************************************/
CANframe::CANframe(uint32_t id, uint8_t dlc, uint8_t data[])
{
        id = CANframe::checkId(id, CAN_FORMAT_EXT);
        CANframe::init(id, dlc, data, CAN_FORMAT_EXT, CAN_FRAME_DAT);
}

/************************************************************************/
/* CANframe - Class constructor.                                        */
/*                                                                      */
/* @param id: Frame ID.                                                 */
/* @param dlc: Frame data length.                                       */
/* @param data: Frame data.                                             */
/* @param format: Standard or extended format? Supported types are      */
/*        CAN_FORMAT_STD (11-bit ID) vs CAN_FORMAT_EXT (29-bit ID).     */
/************************************************************************/
CANframe::CANframe(uint32_t id, uint8_t dlc, uint8_t data[], CAN_FORMAT format)
{
        id = CANframe::checkId(id, format);
        CANframe::init(id, dlc, data, format, CAN_FRAME_DAT);
}

/************************************************************************/
/* CANframe - Class constructor.                                        */
/*                                                                      */
/* @param id: Frame ID.                                                 */
/* @param dlc: Frame data length.                                       */
/* @param data: Frame data.                                             */
/* @param format: Standard or extended format? Supported types are      */
/*        CAN_FORMAT_STD (11-bit ID) vs CAN_FORMAT_EXT (29-bit ID).     */
/* @param frame: Data or request frame? Supported types are             */
/*        CAN_FRAME_DATA (contains data) vs CAN_FRAME_REQ  (no data).   */
/************************************************************************/
CANframe::CANframe(uint32_t id, uint8_t dlc, uint8_t data[],
        CAN_FORMAT format, CAN_FRAME frame)
{
        id = CANframe::checkId(id, format);
        CANframe::init(id, dlc, data, format, frame);
}

/************************************************************************/
/* init - Private object initialization help function.                  */
/*                                                                      */
/* @param id: Frame ID.                                                 */
/* @param dlc: Frame data length.                                       */
/* @param data: Frame data.                                             */
/* @param format: Standard or extended format? Supported types are      */
/*        CAN_FORMAT_STD (11-bit ID) vs CAN_FORMAT_EXT (29-bit ID).     */
/* @param frame: Data or request frame? Supported types are             */
/*        CAN_FRAME_DATA (contains data) vs CAN_FRAME_REQ (no data).    */
/************************************************************************/
void CANframe::init(uint32_t id, uint8_t dlc, uint8_t data[],
        CAN_FORMAT format, CAN_FRAME frame)
{
        this->id = id;
        CANframe::setFormat(format);
        CANframe::setFrameType(frame);
        CANframe::setData(dlc, data);
}

/************************************************************************/
/* getData - Get frame data.                                            */
/*                                                                      */
/* @return The data as an 8-bit array.                                  */
/************************************************************************/
uint8_t* CANframe::getData(void)
{
        return data;
}

/************************************************************************/
/* getId - Get frame ID.                                                */
/*                                                                      */
/* @return The frame ID (32-bit unsigned int).                          */
/************************************************************************/
uint32_t CANframe::getId(void)
{
        return id;
}

/************************************************************************/
/* getLength - Get frame data length.                                   */
/*                                                                      */
/* @return The frame data length (8-bit unsigned int).                  */
/************************************************************************/
uint8_t CANframe::getLength(void)
{
        return dlc;
}

/************************************************************************/
/* getTimeStamp - Get frame time stamp.                                 */
/*                                                                      */
/* @return The frame time stamp (32-bit unsigned int).                  */
/************************************************************************/
uint32_t CANframe::getTimeStamp(void)
{
        return ts;
}

/************************************************************************/
/* isExtended - Returns whether the frame is of extended format type.   */
/*                                                                      */
/* @return True if yes, false if no.                                    */
/************************************************************************/
bool CANframe::isExtended(void)
{
        return ext_fr;
}

/************************************************************************/
/* isRequest - Returns whether the frame is a request frame or not.     */
/*                                                                      */
/* @return True if yes, false if no.                                    */
/************************************************************************/
bool CANframe::isRequest(void)
{
        return req_fr;
}

/************************************************************************/
/* setData - Set frame data.                                            */
/*                                                                      */
/* @param dlc: Frame data length.                                       */
/* @param data: Frame data.                                             */
/************************************************************************/
void CANframe::setData(uint8_t dlc, uint8_t data[])
{
        CANframe::setLength(dlc);
        for (uint8_t i = 0; i < NBR_OF_BYTES; i++) {
                if (!req_fr && i < this->dlc)
                        this->data[i] = data[i];
                else
                        this->data[i] = 0;
        }
}

/************************************************************************/
/* setFormat - Set the format of the frame to be Standard or Extended.  */
/*                                                                      */
/* @param format: Desired format. Supported types are CAN_FORMAT_STD    */
/*                (11-bit ID) vs CAN_FORMAT_EXT (29-bit ID).            */
/************************************************************************/
void CANframe::setFormat(CAN_FORMAT format)
{
        ext_fr = (format == CAN_FORMAT_EXT) ? true : false;
}

/************************************************************************/
/* setFrameType - Set the frame type to be Data frame or Request frame. */
/*                                                                      */
/* @param frame: Desired frame type. Supported types are CAN_FRAME_DAT  */
/*               (contains data) vs CAN_FRAME_REQ (contains no data).   */
/************************************************************************/
void CANframe::setFrameType(CAN_FRAME frame)
{
        req_fr = (frame == CAN_FRAME_REQ) ? true : false;
}

/************************************************************************/
/* setId - Set frame ID.                                                */
/*                                                                      */
/* @param id: Desired ID.                                               */
/*                                                                      */
/* @return CAN_OK if desired ID is within supported range,              */
/*         CAN_VAL_OOR otherwise.                                       */
/************************************************************************/
CAN_RES CANframe::setId(uint32_t id)
{
        CAN_FORMAT format = ext_fr ? CAN_FORMAT_EXT : CAN_FORMAT_STD;
        uint32_t new_id = CANframe::checkId(id, format);

        if (new_id != id) {
                return CAN_VAL_OOR;
        } else {
                this->id = new_id;
                return CAN_OK;
        }
}

/************************************************************************/
/* setLength - Set frame data length.                                   */
/*                                                                      */
/* @param dlc: Desired data length in bytes. A maximum of 8 bytes are   */
/*             allowed per frame. If a higher value is entered, it      */
/*             will be set to 8.                                        */
/************************************************************************/
void CANframe::setLength(uint8_t dlc)
{
        this->dlc = (dlc <= NBR_OF_BYTES) ? dlc : NBR_OF_BYTES;
}

/************************************************************************/
/* setTimeStamp - Set time stamp.                                       */
/*                                                                      */
/* @param ts: Time stamp of frame arrival.                              */
/************************************************************************/
void CANframe::setTimeStamp(uint32_t ts)
{
        this->ts = ts;
}

/************************************************************************/
/* Private ID length check function.                                    */
/*                                                                      */
/* @param id: ID to check.                                              */
/* @param format: CAN format to check ID length for.                    */
/************************************************************************/
uint32_t CANframe::checkId(uint32_t id, CAN_FORMAT format)
{
        if (format == CAN_FORMAT_EXT) {
                if (id > MAX_ID_EXT)
                        return MAX_ID_EXT;
        } else {
                if (id > MAX_ID_STD)
                        return MAX_ID_STD;
        }
        return id;
}
