/*
 * CANframe.h
 *
 * This file is part of the Wrafter ARC CAN bus library.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: David Bengtsson
 */

#ifndef CANFRAME_H
#define CANFRAME_H

#include "util/conf.h"

extern const uint16_t MAX_ID_STD;       /* Standard format max ID */
extern const uint32_t MAX_ID_EXT;       /* Extended format max ID */

/************************************************************************/
/* CAN frame class.                                                     */
/************************************************************************/
class CANframe
{
        private:
        uint32_t id;            /* Frame ID */
        uint8_t dlc;            /* Frame data length */
        uint8_t data[8];        /* Frame data */
        bool ext_fr;            /* Extended format flag */
        bool req_fr;            /* Request frame flag */
        uint32_t ts;            /* Time stamp */

        void init(uint32_t, uint8_t, uint8_t *, CAN_FORMAT, CAN_FRAME);
        uint32_t checkId(uint32_t, CAN_FORMAT);

        public:
        CANframe(void);
        CANframe(uint32_t);
        CANframe(uint32_t, uint8_t, uint8_t *);
        CANframe(uint32_t, uint8_t, uint8_t *, CAN_FORMAT);
        CANframe(uint32_t, uint8_t, uint8_t *, CAN_FORMAT, CAN_FRAME);

        uint8_t* getData(void);
        uint32_t getId(void);
        uint8_t getLength(void);
        uint32_t getTimeStamp(void);

        bool isExtended(void);
        bool isRequest(void);

        void setData(uint8_t, uint8_t *);
        void setFormat(CAN_FORMAT);
        void setFrameType(CAN_FRAME);
        CAN_RES setId(uint32_t);
        void setLength(uint8_t);
        void setTimeStamp(uint32_t);
};

typedef void (*voidFuncPtr)(CANframe &);        /* Function pointer alias */

#endif
