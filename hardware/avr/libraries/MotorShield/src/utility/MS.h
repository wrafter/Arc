/*
 * Copyright (c) 2016 by Simon Wrafter <simon.wrafter@gmail.com>
 * Power Stage Controller library for Wrafter ARC.
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU General Public License version 2
 * or the GNU Lesser General Public License version 2.1, both as
 * published by the Free Software Foundation.
 */

#ifndef _MS_C_H_INCLUDED
#define _MS_C_H_INCLUDED

#define SHUTDOWN_PIN 4

typedef struct MS_MAP {
	uint16_t max_rpm; //max RPM
	uint16_t RPM*; // vector at RPMs to map for
	float ampAtRPM*; //wanted sinus amplitude at RPMs
	uint16_t nbrOfPoints;
} MS_MAP;

void MS_mapMotor(MS_MAP map);
void MS_stop(void);
void MS_timer_int(void);
float MS_set_rpm(uint16_t rpm);
float MS_get_rpm(void);
uint8_t MS_get_emf_encode(void);
uint16_t MS_get_emf_angle(void);

#endif //_MS_C_H_INCLUDED
