/*
 * Copyright (c) 2016 by Simon Wrafter <simon.wrafter@gmail.com>
 * Motor Shield library for Wrafter ARC.
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU General Public License version 2
 * or the GNU Lesser General Public License version 2.1, both as
 * published by the Free Software Foundation.
 */

#include "Arduino.h"
#include "MS.h"
#include "../../PSC/src/utility/PSC_C.h"

volatile uint16_t _timer_counter = 0;
volatile uint16_t _timer_limit = 0;
volatile int8_t _step_size = 0;
float _rpm = 0;
MS_MAP _map;


void MS_mapMotor(MS_MAP map) {
	_map = map;
}

void MS_stop(void) {
	digitalWrite(SHUTDOWN_PIN, HIGH); //shutdown
	PCTL &= ~_BV(PRUN); //PSC.stop()
}

void MS_timer_int(void) {
	_timer_counter++;
	if (_timer_counter >= _timer_limit) {
		psc_moveAngle(_step_size);
		_timer_counter = 0;
	}
}

float MS_set_rpm(int16_t rpm) {
	uint8_t sign = 1;
	
	if (rpm < 0) {
		sign = 0;
		rpm *= -1;
	}
	
	if (rpm == 0) {
		best_count = 1;
		best_step = 0;
		psc_setAmplitude(0);
		return _rpm = 0;
	}
	
	float count_over_step = 375.0 / rpm;
	uint8_t step[7] = [1, 2, 5, 10, 20, 50, 100];
	//res          1600 800 320 160 80  32  16
	float best_error = 1;
	uint8_t best_step = 0;
	uint16_t best_count = 0;
	uint8_t i_lim = 7;
	
	if (rpm < 60) {
		i_lim = 3
	} else if (rpm < 300) {
		i_lim = 5;
	}
	
	if (rpm > _map.maxRPM) {
		rpm = _map.maxRPM;
	}
	
	for (int i = 0; i<i_lim; i++) {
		float count = step[i] * count_over_step;
		
		float error = count % 1;
		if (error > 0.5) {
			error = 1 - error;
		}
		if (error < best_error) {
			best_step = step[i];
			best_count = (uint16_t) round(count);
			best_error = error;
		}
		
		if (error < 0.001) {
			break;
		}
	}
	_timer_limit = best_count;
	_step_size = best_step * sign;
	
	for (uint16_t i=0; i<_map.nbrOfPoints; i++) {
		if (rpm < _map.RPM[i]) {
			//<0 rpm and maxRPM not specifically handled
			float k = (_map.ampAtRPM[i] - _map.ampAtRPM[i-1]) / (_map.RPM[i] - _map.RPM[i-1]);
			float m = _map.ampAtRPM[i] - k * _map.RPM[i];
			psc_setAmplitude(k * rpm + m);
		}
	}
	
	return _rpm = best_step / best_count * 375.0;
}

float MS_get_rpm(void) {
	return _rpm;
}

uint8_t MS_get_emf_encode(void) {
	return (digitalRead(A3) << 2) | (digitalRead(A4) << 1) | digitalRead(A5);
}

uint16_t MS_get_emf_angle(void) {
	uint8_t encode = (digitalRead(A3) << 2) | (digitalRead(A4) << 1) | digitalRead(A5);
	
	if (encode == 0b00000001) {
		return 0;
	} else if (encode == 0b00000011) {
		return 267;
	} else if (encode == 0b00000010) {
		return 533;
	} else if (encode == 0b00000110) {
		return 800;
	} else if (encode == 0b00000100) {
		return 1067;
	} else if (encode == 0b00000101) {
		return 1333;
	}
}
