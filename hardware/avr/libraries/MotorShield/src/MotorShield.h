/*
 * Copyright (c) 2016 by Simon Wrafter <simon.wrafter@gmail.com>
 * Power Stage Controller library for Wrafter ARC.
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU General Public License version 2
 * or the GNU Lesser General Public License version 2.1, both as
 * published by the Free Software Foundation.
 */

#ifndef _MS_H_INCLUDED
#define _MS_H_INCLUDED

//modes
#define MS_OFF   0
#define MS_MOTOR 1
//#define MS_PWM   2

//phases
#define MS_PHASE_0 0
#define MS_PHASE_1 1
#define MS_PHASE_2 2

//inputs
#define MS_INPUT_IGNORE 0
#define MS_COMPARATOR 1
#define MS_EXTERNAL 2

class MS_Class {
public:
	void begin(uint8_t mode);
	void begin(uint8_t mode, uint8_t input);
	void start();
	void stop();
	void end();
	void shutdown(boolean val);

	void lock();
	void unlock();

	uint8_t getMode();
	void setRPM(int16_t rpm);
	int16_t getRPM();
	uint16_t getTemperature();

	void setEMFInterrupts(boolean val);
	uint8_t getEMFPosition();

	void attachInterrupt(void (*userFunc)(void));
	void detachInterrupt();

};

extern MS_Class MotorShield;

#endif // _MS_H_INCLUDED
