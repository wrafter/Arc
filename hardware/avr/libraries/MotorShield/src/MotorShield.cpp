/*
 * Copyright (c) 2016 by Simon Wrafter <simon.wrafter@gmail.com>
 * Power Stage Controller library for Wrafter ARC.
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU General Public License version 2
 * or the GNU Lesser General Public License version 2.1, both as
 * published by the Free Software Foundation.
 */

#include "Arduino.h"
#include "PSC.h"
#include "MotorShield.h"

extern "C" {
	#include "utility/MS.h"
}

MS_Class MotorShield;

void MS_Class::begin(uint8_t mode) {
	begin(mode, MS_COMPARATOR);
}

void MS_Class::begin(uint8_t mode, uint8_t input) {
	PSC.begin(mode, input);
	if(input == MS_COMPARATOR) {
		//start comparators, AVcc/1.6 = approx. 75 C
		pinMode(AC0, VREF4);
		pinMode(AC1, VREF4);
		pinMode(AC2, VREF4);
	} else {
		pinMode(7, INPUT); // configure unused comparator inputs
		pinMode(A0, INPUT); // analog inputs can be used to read temperature
		pinMode(A2, INPUT); // should not be used together with comparators!

		pinMode(8, INPUT); // PSCIN0
		pinMode(9, INPUT); // PSCIN1
		pinMode(6, INPUT); // PSCIN2
	}
	pinMode(SHUTDOWN_PIN, OUTPUT); //shutdown
	shutdown(true);

	pinMode(A3, INPUT); // back-EMF, phase 2
	pinMode(A4, INPUT); // back-EMF, phase 1
	pinMode(A5, INPUT); // back-EMF, phase 0

	PSC.attachInterrupt(PSC_FAULT_INT, MS_stop);
	PSC.attachInterrupt(PSC_END_INT, MS_timer_int);

	interrupts();
}

void MS_Class::begin(uint8_t mode, uint8_t input, MS_MAP map) {
	begin(mode, input);
	MS_mapMotor(map);
}

void MS_Class::start() {
	PSC.start();
	shutdown(false);
}

void MS_Class::stop() {
	PSC.stop();
	shutdown(true);
}

void MS_Class::end() {
	shutdown(true);
	PSC.end();
	//release comparators
	pinMode(7, INPUT); // configure unused comparator inputs
	pinMode(A0, INPUT); // analog inputs can be used to read temperature
	pinMode(A2, INPUT); // should not be used together with comparators!
	//release interrupts
	PSC.detachInterrupt(PSC_END_INT);
	PSC.detachInterrupt(PSC_FAULT_INT);
}

void MS_Class::shutdown(boolean val) {
	digitalWrite(SHUTDOWN_PIN, val ? HIGH : LOW);
}

void MS_Class::lock() {
	PSC.lock();
}
void MS_Class::unlock() {
	PSC.unlock();
}

uint8_t MS_Class::getMode() {
	return PSC.getMode();
}

float MS_Class::setRPM(int16_t rpm) {
	return MS_set_rpm(rpm);
}

float MS_Class::getRPM() {
	return MS_get_rpm();
}

uint16_t MS_Class::getTemperature() {
	if(!(PMIC1 & _BV(PISEL1))) { //not using comparators
		return analogRead(A0);
	}
	return 0;
}

void MS_Class::setEMFInterrupts(boolean val) {
	if (val) {
		PCMSK2 |= (1<<PCINT22);
		PCMSK1 |= (1<<PCINT12) | (1<<PCINT13);
		PCICR |= (1<<PCIE1) | (1<<PCIE2);
	} else {
		PCMSK2 &= ~(1<<PCINT22);
		PCMSK1 &= ~((1<<PCINT12) | (1<<PCINT13));
		if (!PCMSK1) {
			PCICR &= ~(1<<PCIE1);
		}
		if (!PCMSK2) {
			PCICR &= ~(1<<PCIE2);
		}
	}
}

uint8_t MS_Class::getEMFPosition() {
	//read pins, build byte, return
	return MS_get_emf_pos();
}

void MS_Class::attachInterrupt(void (*userFunc)(void)) {
	PSC.attachInterrupt(PSC_FAULT_INT, userFunc);
}

void MS_Class::detachInterrupt() {
	PSC.detachInterrupt(PSC_FAULT_INT);
}
