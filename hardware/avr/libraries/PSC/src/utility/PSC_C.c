/*
 * Copyright (c) 2016 by Simon Wrafter <simon.wrafter@gmail.com>
 * Power Stage Controller library for Wrafter ARC.
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU General Public License version 2
 * or the GNU Lesser General Public License version 2.1, both as
 * published by the Free Software Foundation.
 */

#include "Arduino.h"
#include "PSC_C.h"

uint8_t _mode;
uint16_t _angle;
float _amplitude;

float psc_fastSin(float x)

void psc_begin(uint8_t mode, uint8_t input) {
	//Start PLL
	PLLCSR = _BV(PLLF) | _BV(PLLE);
	while (!(PLLCSR & _BV(PLOCK))) {}

	if (mode == PSC_MOTOR) {
		_mode = PSC_MOTOR;

		PMIC0 = _BV(PFLTE0);
		PMIC1 = _BV(PFLTE1);
		PMIC2 = _BV(PFLTE2);

		PCTL = _BV(PPRE0) | _BV(PCLKSEL); //PLL, div by 4 => 16 MHz clock
		POCR_RB = 0x640; //1600

		POC = 0x3F; //enable all outputs
	} else if(mode == PSC_PWM) {
		_mode = PSC_PWM;

		PMIC0 = _BV(POVEN0) | _BV(PELEV0) | _BV(PFLTE0);
		PMIC1 = _BV(POVEN1) | _BV(PELEV1) | _BV(PFLTE1);
		PMIC2 = _BV(POVEN2) | _BV(PELEV2) | _BV(PFLTE2);

		PCTL = _BV(PPRE1) | _BV(PPRE0) | _BV(PCLKSEL) | _BV(PRUN); //PLL, div by 256 => 250 kHz clock & start (PRUN)
		POCR_RB = 0x00FF; //255
	}

	PCNF = _BV(POPA) | _BV(POPB);

	if (input == PSC_COMPARATOR) {
		PMIC0 |= _BV(PISEL0) | _BV(PRFM02);
		PMIC1 |= _BV(PISEL1) | _BV(PRFM12);
		PMIC2 |= _BV(PISEL2) | _BV(PRFM22);
	} else if (input == PSC_EXTERNAL) {
		PMIC0 |= _BV(PRFM02);
		PMIC1 |= _BV(PRFM12);
		PMIC2 |= _BV(PRFM22);
	} // else if (input == PSC_INPUT_IGNORE)
}

void psc_start(void) {
	PCTL |= _BV(PRUN);
}

void psc_stop(void) {
	PCTL &= ~_BV(PRUN);
}

void psc_end(void) {
	//Stop PSC
	PMIC0 = 0x00;
	PMIC1 = 0x00;
	PMIC2 = 0x00;
	POC = 0x00;
	PCTL = 0x00;
	PIM = 0x00;
	//Stop PLL
	PLLCSR = 0x00;
	_mode = PSC_OFF;
}

void psc_powerWrite(uint16_t angle, float amplitude) {
	if (_mode == PSC_MOTOR) {
		float angle_1 = ((float) angle) / 1600.0;
		float angle_2 = (angle_1 + 1.0/3.0); //add 2/3*pi
		float angle_3 = (angle_1 + 2.0/3.0); //add 4/3*pi

		_angle = angle_1;
		_amplitude = (amplitude > 1 ? 1 : amplitude) * 1600.0; //limit amplitude

		if(angle_2 > 1.0) {
			angle_2 -= 1.0;
		}
		if(angle_3 > 1.0) {
			angle_3 -= 1.0;
		}

		int16_t phase_1 = (int16_t) (psc_fastSin(angle_1) * _amplitude);
		int16_t phase_2 = (int16_t) (psc_fastSin(angle_2) * _amplitude);
		int16_t phase_3 = (int16_t) (psc_fastSin(angle_3) * _amplitude);

		if (phase_1 >= 0) {
			POCR0SA = 0;
			POCR0RA = phase_1;
			POCR0SB = 1600;
		} else {
			POCR0SA = 0;
			POCR0RA = 0;
			POCR0SB = 1600 + phase_1;
		}
		if (phase_2 >= 0) {
			POCR1SA = 0;
			POCR1RA = phase_2;
			POCR1SB = 1600;
		} else {
			POCR1SA = 0;
			POCR1RA = 0;
			POCR1SB = 1600 + phase_2;
		}
		if (phase_3 >= 0) {
			POCR2SA = 0;
			POCR2RA = phase_3;
			POCR2SB = 1600;
		} else {
			POCR2SA = 0;
			POCR2RA = 0;
			POCR2SB = 1600 + phase_3;
		}
	}
}

void psc_analogWrite(uint8_t pin, uint8_t val) {
	if (_mode == PSC_PWM) {
		switch (pin) {
			case 10: //PSCOUT0A
				POC |= _BV(POEN0A);
				POCR0SA = 0;
				POCR0RA = val & 0xFF;
				break;
			case 13: //PSCOUT0B
				POC |= _BV(POEN0B);
				POCR0SB = 255-(val & 0xFF);
				break;
			case 3: //PSCOUT1A
				POC |= _BV(POEN1A);
				POCR1SA = 0;
				POCR1RA = val & 0xFF;
				break;
			case 5: //PSCOUT1B
				POC |= _BV(POEN1B);
				POCR1SB = 255-(val & 0xFF);
				break;
			case 12: //PSCOUT2A
				POC |= _BV(POEN2A);
				POCR2SA = 0;
				POCR2RA = val & 0xFF;
				break;
			case 11: //PSCOUT2B
				POC |= _BV(POEN2B);
				POCR2SB = 255-(val & 0xFF);
				break;
		}
	}
}

void psc_release(uint8_t pin) {
	if (_mode == PSC_PWM) {
		switch (pin) {
			case 10: //PSCOUT0A
				POC &= ~_BV(POEN0A);
				POCR0SA = 0;
				POCR0RA = 0;
				break;
			case 13: //PSCOUT0B
				POC &= ~_BV(POEN0B);
				POCR0SB = 255;
				break;
			case 3: //PSCOUT1A
				POC &= ~_BV(POEN1A);
				POCR1SA = 0;
				POCR1RA = 0;
				break;
			case 5: //PSCOUT1B
				POC &= ~_BV(POEN1B);
				POCR1SB = 255;
				break;
			case 12: //PSCOUT2A
				POC &= ~_BV(POEN2A);
				POCR2SA = 0;
				POCR2RA = 0;
				break;
			case 11: //PSCOUT2B
				POC &= ~_BV(POEN2B);
				POCR2SB = 255;
				break;
		}
	}
}

uint16_t psc_getAngle() {
	return _angle;
}

float psc_setAmplitude(float amplitude) {
	return _amplitude = amplitude;
}

float psc_getAmplitude() {
	return _amplitude;
}

int16_t psc_moveAngle(int16_t move) {
	psc_powerWrite(_angle + move, _amplitude);
	return _angle;
}

void psc_lock() {
	PCNF |= _BV(PULOCK);
}

void psc_unlock() {
	PCNF &= ~_BV(PULOCK);
}

uint8_t psc_getMode() {
	return _mode;
}

boolean psc_getExtEvent0() {
	boolean status = PIFR & _BV(PEV0) ? true : false;
	PIFR |= _BV(PEV0); //clear bit
	return status;
}

boolean psc_getExtEvent1() {
	boolean status = PIFR & _BV(PEV1) ? true : false;
	PIFR |= _BV(PEV1); //clear bit
	return status;
}

boolean psc_getExtEvent2() {
	boolean status = PIFR & _BV(PEV2) ? true : false;
	PIFR |= _BV(PEV2); //clear bit
	return status;
}

boolean psc_getEndOfCycle() {
	boolean status = PIFR & _BV(PEOP) ? true : false;
	PIFR |= _BV(PEOP); //clear bit
	return status;
}

// Fast sin(2*pi*x) approximation.
// The caller must make sure that the argument lies within [0..1).
// Courtesy of:
// https://namoseley.wordpress.com/2012/09/18/a-quick-and-dirty-sinx-approximation/
float psc_fastSin(float x){
	if (x < 0.5 ) {
		return -16.0*x*x + 8.0*x;
	}
	else {
		return 16.0*x*x - 24.0*x + 8.0;
	}
}

///////////////////////////////

typedef void (*voidFuncPtr)(void);

static void nothing(void) {}

voidFuncPtr funcVector[PSC_NUM_INTERRUPTS] = {
	nothing,
	nothing
};

void psc_attachInterrupt(uint8_t interruptNum, void (*userFunc)(void)) {
	if (interruptNum < PSC_NUM_INTERRUPTS) {
		if (interruptNum == PSC_END_INT) {
			PIM |= _BV(PEOPE);
		} else if (interruptNum == PSC_FAULT_INT) {
			PIM |= _BV(PEVE2) | _BV(PEVE1) | _BV(PEVE0);
		}
		funcVector[interruptNum] = userFunc;
	}
}

void psc_detachInterrupt(uint8_t interruptNum) {
	if(interruptNum < PSC_NUM_INTERRUPTS) {
		if (interruptNum == PSC_END_INT) {
			PIM &= ~(_BV(PEOPE));
		} else if (interruptNum == PSC_FAULT_INT) {
			PIM &= ~(_BV(PEVE2) | _BV(PEVE1) | _BV(PEVE0));
		}
		funcVector[interruptNum] = nothing;
	}
}

ISR(PSC_FAULT_vect) {
	funcVector[PSC_FAULT_INT]();
}

ISR(PSC_EC_vect) {
	funcVector[PSC_END_INT]();
}
