/*
 * Copyright (c) 2016 by Simon Wrafter <simon.wrafter@gmail.com>
 * Power Stage Controller library for Wrafter ARC.
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU General Public License version 2
 * or the GNU Lesser General Public License version 2.1, both as
 * published by the Free Software Foundation.
 */

#ifndef _PSC_H_INCLUDED
#define _PSC_H_INCLUDED

//modes
#define PSC_OFF   0
#define PSC_MOTOR 1
#define PSC_PWM   2

//inputs
#define PSC_INPUT_IGNORE 0
#define PSC_COMPARATOR 1
#define PSC_EXTERNAL 2

//interrupts
#define PSC_NUM_INTERRUPTS 2
#define PSC_END_INT 0
#define PSC_FAULT_INT 1

class PSC_Class {
public:
	// Initialize the PSC library

	void begin(uint8_t mode);
	void begin(uint8_t mode, uint8_t input);
	void start();
	void stop();
	void end();

	void powerWrite(uint16_t angle, float amplitude);
	void analogWrite(uint8_t pin, uint8_t val); //PWM mode
	void release(uint8_t pin); //PWM mode

	uint16_t getAngle();
	float getAmplitude();
	int16_t moveAngle(int16_t move);

	void lock();
	void unlock();

	uint8_t getMode();

	boolean getExtEvent0();
	boolean getExtEvent1();
	boolean getExtEvent2();
	boolean getEndOfCycle();

	void attachInterrupt(uint8_t interruptNum, void (*userFunc)(void));
	void detachInterrupt(uint8_t interruptNum);
};

extern PSC_Class PSC;

#endif
